from mlp.perceptron import MLP
from mlp.layer import Layer
from mlp.activation import Sigmoid, Linear, ReLU, TanH, ATan
from mlp.error import ErrSum
from numpy import genfromtxt
from typing import List
import numpy as np
import matplotlib.pyplot as plt

file = './data/classification/data.three_gauss.train.100.csv'
data = genfromtxt(file, delimiter=',', skip_header=True)
X=data[:,:-1]
Y=data[:,-1]-1
inputs_count = len(X[0])
classes = int(Y.max())+1

test_file = './data/classification/data.three_gauss.test.100.csv'
test_data = genfromtxt(test_file, delimiter=',', skip_header=True)
test_X=test_data[:,:-1]
test_Y=test_data[:,-1]-1
classes = int(Y.max())+1
print('train file:', file)
print('test file:', test_file)

momentum_rate = 0.5
print('momentum rate:', momentum_rate  )

learning_rate=0.002
print('learning rate:', learning_rate  )

iters = 2
print('iters:', iters)

experiments = 1
print('experiments:', experiments)

np.random.seed(0)

accuracy = []
test_accuracy = []

functions = [
    TanH(),
    ATan(),
    Sigmoid()
]

layers_collection = [
    [inputs_count, classes],
    [inputs_count, 2, classes],
    [inputs_count, 20, classes],
    [inputs_count, 100, classes],
    [inputs_count, 20, 10, classes],
    [inputs_count, 10, 20, classes],
    [inputs_count, 20, 50, 20, classes],
    [inputs_count, 20, 50, 50, 20, classes],
]

print(functions)

for layers in layers_collection:
    print('-------------------------')
    print('layers:', layers)
    current_accuracy = []
    current_test_accuracy = []
    for k in range(0, experiments):
        perceptron = MLP(
            layers=layers,
            learning_rate=learning_rate,
            momentum_rate=momentum_rate, 
            problem_type='classification',
            activation_fn=Sigmoid(),
            error_fn=ErrSum(),
            batch_learning=False,
            use_bias=False,
            iters=iters, 
            seed=np.random.randint(0, 100))
        perceptron.add_layer(neurons_cnt=classes, activation_fn=Sigmoid(), use_bias = False)
        perceptron.train(X, Y, show_error=True, show_set=True)
        score = 0
        test_score = 0
        for i in range(Y.size) :
            if (perceptron.predict(X[i]) == Y[i]):
                score += 1
        current_accuracy.append(score/Y.size)
        for i in range(test_Y.size) :
            if (perceptron.predict(test_X[i]) == test_Y[i]):
                test_score += 1
        current_accuracy.append(score/Y.size)
        current_test_accuracy.append(test_score/test_Y.size)
    mean = np.mean(current_accuracy)
    test_mean = np.mean(current_test_accuracy)
    variance = np.var(current_accuracy)
    test_variance = np.var(current_test_accuracy)
    print('y (current accuracy): ', mean, variance)
    print('y (current test accuracy): ', test_mean, test_variance)
    accuracy.append([mean, variance])
    test_accuracy.append([test_mean, test_variance])
    print('------', flush=True)

print('-------------------------')
print('y (accuracy): ', accuracy)
print('y (test accuracy): ', test_accuracy)