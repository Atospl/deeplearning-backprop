"""
Contains definition of class representing single layer of MLP
"""
import numpy as np


from mlp.activation import Activation


class Layer:

    def __init__(self, inputs_cnt: int, neurons_cnt: int, activation_fn: Activation, use_bias: bool = True) -> None:
        self.inputs_cnt = inputs_cnt
        self.neurons_cnt = neurons_cnt
        self.activation_fn = activation_fn

        self.input = np.zeros(inputs_cnt)
        self.lin = np.zeros(neurons_cnt)

        self.weights = self.__initialize_weights(neurons_cnt, inputs_cnt, activation_fn)
        self.delta = np.zeros((neurons_cnt, inputs_cnt))

        self.use_bias = use_bias
        if use_bias:
            self.bias = self.__initialize_biases(neurons_cnt)
            self.bias_delta = np.zeros(neurons_cnt)


    def linear_transform(self, input: np.array) -> np.array:
        """
        Input x weights + bias
        """
        if self.use_bias:
            return self.weights@input + self.bias
        else:
            return self.weights@input

    def forward(self, layer_input: np.array) -> (np.array, np.array):
        """
        Forward propagation through layer
        """
        self.input = layer_input
        self.lin = self.linear_transform(layer_input)
        return self.get_layer_output(self.lin)

    def get_layer_output(self, x) -> np.array:
        """
        Output of layer - forward pass transformed by activation fn
        :return: output array of transformed input
        """
        return np.vectorize(self.activation_fn)(x)

    def __initialize_weights(self, neurons_cnt: int, inputs_cnt: int, activation_fn: Activation) -> np.array:
        """
        Simply draws from standard normal distribution
        :param neurons_cnt: neurons in layer
        :param inputs_cnt: size of previous layer
        :param activation_fn: output activation function
        :return: numpy array representing initial weights
        """
        return np.random.randn(neurons_cnt, inputs_cnt)

    def __initialize_biases(self, neurons_cnt) -> np.array:
        """
        Returns array with initial bias values
        :param neurons_cnt: size of array
        :return: array with bias init values
        """
        return np.random.uniform(0, 1, size=neurons_cnt)

    def update_weights(self, weights_correction: np.array, correction: np.array, learning_rate: float, 
            momentum_rate: float, bias_learning_rate: float, bias_momentum_rate: float ):
        self.delta = momentum_rate*self.delta + (1 - momentum_rate)*weights_correction
        self.weights += learning_rate * self.delta
        if self.use_bias:
            self.bias_delta = bias_momentum_rate*self.bias_delta + (1 - bias_momentum_rate)*correction
            self.bias += bias_learning_rate * self.bias_delta

    def get_correction(self, factor: np.array) -> (np.array, np.array):
        correction = np.vectorize(self.activation_fn.df)(self.lin)*factor
        weights_correction = np.outer(correction, self.input)
        return weights_correction, correction