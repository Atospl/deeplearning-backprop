"""
Module contains definition of class representing MultiLayerPerceptron
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt


from typing import List
from mlp.layer import Layer
from mlp.activation import Activation
from mlp.error import ErrorFunction

class MLP:
    """
    MultiLayerPerceptron
    """

    def __init__(self,
                layers: List[int],
                problem_type: str,
                activation_fn: Activation,
                error_fn: ErrorFunction,
                learning_rate: float = 1,
                momentum_rate: float = 0,
                use_bias = False,
                bias_learning_rate: float = None, 
                bias_momentum_rate: float = None,
                batch_learning: bool = True,
                iters: int = 100,
                seed: float = 0) :

        self.input_size = layers[0]
        self.problem_type = problem_type
        self.activation_fn = activation_fn
        self.error_fn = error_fn
        self.learning_rate = learning_rate
        self.momentum_rate = momentum_rate
        self.use_bias = use_bias
        self.bias_learning_rate = bias_learning_rate if bias_learning_rate else learning_rate
        self.bias_momentum_rate = bias_momentum_rate if bias_momentum_rate else momentum_rate
        self.batch_learning = batch_learning
        self.iters = iters
        self.seed = seed
        
        np.random.seed(seed)

        self.__initialize_layers(layers)

    def add_layer(self, neurons_cnt: int, activation_fn: Activation, use_bias: bool = True):
        inputs_cnt = self.input_size if not self.layers else self.layers[-1].neurons_cnt
        self.layers.append(Layer(inputs_cnt, neurons_cnt, activation_fn, use_bias))

    def __initialize_layers(self, layers: List[int]):
        self.layers = []
        for inputs, neurons in zip(layers[:-1], layers[1:]):
            self.layers.append(Layer(inputs, neurons, self.activation_fn, self.use_bias))

    def train(self, X: np.array, Y: np.array, show_error: bool=False, show_set: bool=False):
        error_vals = []
        if self.problem_type == 'classification':
            classes = int(Y.max()+1)
            Y_org = Y if show_set else None
            Y = np.eye(classes)[Y.astype(int)]
        if self.batch_learning:
            batch_size = np.size(X, 0)
            weights_corrections = np.array([np.zeros([layer.neurons_cnt, layer.inputs_cnt]) for layer in self.layers])
            corrections = np.array([np.zeros(layer.neurons_cnt) for layer in self.layers])
            for _ in range(0, self.iters):
                tmp_error_vals = []
                for (x, y) in zip(X, Y):
                    output = self.forward(x)
                    tmp_error_vals.append(self.error_fn(y, output))
                    factor = self.error_fn.df(y, output)
                    for i in range(-1, -len(self.layers) - 1, -1):
                        weights_correction, correction = self.layers[i].get_correction(factor)
                        weights_corrections[i] += weights_correction
                        if self.layers[i].use_bias:
                            corrections[i] += correction
                        factor = self.layers[i].weights.transpose()@correction
                for i in range(-1, -len(self.layers) - 1, -1):
                    self.layers[i].update_weights(weights_corrections[i] / batch_size, corrections[i] / batch_size, self.learning_rate, self.momentum_rate, self.bias_learning_rate, self.bias_momentum_rate)
                error_vals.append(sum(tmp_error_vals))
        else:
            for _ in range(0, self.iters):
                tmp_error_vals = []
                for (x, y) in zip(X, Y):
                    output = self.forward(x)
                    tmp_error_vals.append(self.error_fn(y, output))
                    factor = self.error_fn.df(y, output)
                    for layer in self.layers[::-1]:
                        weights_correction, correction = layer.get_correction(factor)
                        layer.update_weights(weights_correction, correction, self.learning_rate, self.momentum_rate, self.bias_learning_rate, self.bias_momentum_rate)
                        factor = layer.weights.transpose()@correction
                error_vals.append(sum(tmp_error_vals))
        if show_error:
            fig, ax = plt.subplots()
            ax.plot(range(0, self.iters), error_vals)
            ax.set(xlabel='epoch', ylabel='error')
            ax.grid()
            plt.show()
        if show_set:
            predicted = []
            for x in X:
                predicted.append(self.predict(x))
            predicted = np.array(predicted)
            fig, ax = plt.subplots()
            if self.problem_type == 'classification' and np.size(X,1) == 2:
                cmap_light = matplotlib.colors.ListedColormap(['lightblue', 'violet', 'lightcoral','springgreen', 'khaki'])
                cmap_dark  = matplotlib.colors.ListedColormap(['darkblue', 'darkviolet', 'red','darkgreen', 'yellow'])
                mesh_step_size = .01
                x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
                y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
                xx, yy = np.meshgrid(np.arange(x_min, x_max, mesh_step_size), np.arange(y_min, y_max, mesh_step_size))
                Z = np.zeros(xx.shape)
                for x in range(0, xx.shape[0]):
                    for y in range(0, xx.shape[1]):
                        Z[x,y] = self.predict([xx[x,y], yy[x, y]])
                plt.pcolormesh(xx, yy, Z,cmap=cmap_light)
                plt.scatter(X[:,0], X[:,1], c=Y_org, label=Y_org,cmap=cmap_dark,edgecolors='k')
                ax.set(xlabel='X1', ylabel='X2')
            elif np.size(X,1) == 1:
                args = X[:,0].argsort()
                ax.set(xlabel='X', ylabel='Y')
                ax.plot(X[args], Y[args], label='actual' )
                ax.plot(X[args], predicted[args], label='predicted')
                ax.legend()

            ax.grid()
            plt.show()

    def forward(self, x):
        output = x
        for layer in self.layers:
            output = layer.forward(output)
        return output

    def predict(self, X: np.array):
        output = X
        for layer in self.layers:
            output = layer.forward(output)
        if self.problem_type == 'classification':
            return np.argmax(output)
        elif self.problem_type == 'regression':
            return output if self.layers[-1].neurons_cnt > 1 else np.asscalar(output)
        else:
            return None