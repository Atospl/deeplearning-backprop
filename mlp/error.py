"""
Contains definitions of different error functions
"""

import numpy as np


class ErrorFunction:
    """
    Abstract base class for error fn
    """

    def __init__(self, name):
        self.name = name

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        """
        Calculates error function output
        :param actual_value: Actual value
        :param predicted_value: Predicted value
        :return: f(actual_value, predicted_value)
        """
        pass

    def df(self, actual_value: np.array, predicted_value: np.array):
        """
        Calculates error function derivative
        :param actual_value: Actual value
        :param predicted_value: Predicted value
        :return: f'(actual_value, predicted_value)
        """
        pass


class ErrSum(ErrorFunction):

    def __init__(self):
        self.name = 'MSE'

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        return np.sum((actual_value - predicted_value)**2)

    def df(self, actual_value: np.array, predicted_value: np.array):
        return actual_value - predicted_value

class CrossEntropyCost(ErrorFunction):

    def __init__(self):
        self.name = 'Cross Entropy'

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        return predicted_value*np.log(actual_value) + (1-predicted_value)*np.log(1-actual_value)

    def df(self, actual_value: np.array, predicted_value: np.array):
        return (actual_value - predicted_value)/((1-actual_value)*actual_value)

class HellingerDist(ErrorFunction):

    def __init__(self):
        self.name = 'error_sum'

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        return 0.5**2*(actual_value**2-predicted_value**2)

    def df(self, actual_value: np.array, predicted_value: np.array):
        return (actual_value**0.5-predicted_value**0.5)/((2*actual_value)**0.5)

class KullbackLeiblerDiv(ErrorFunction):

    def __init__(self):
        self.name = 'error_sum'

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        return None

    def df(self, actual_value: np.array, predicted_value: np.array):
        return -predicted_value/actual_value

class GenKullbackLeiblerDiv(ErrorFunction):

    def __init__(self):
        self.name = 'error_sum'

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        return None

    def df(self, actual_value: np.array, predicted_value: np.array):
        return 1-predicted_value/actual_value

class ItakuraSaitoDist(ErrorFunction):

    def __init__(self):
        self.name = 'error_sum'

    def __call__(self, actual_value: np.array, predicted_value: np.array):
        return None

    def df(self, actual_value: np.array, predicted_value: np.array):
        return (actual_value-predicted_value)/(actual_value**2)