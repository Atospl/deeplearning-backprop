"""
Contains definitions of different activation functions
"""

import numpy as np


class Activation:
    """
    Abstract base class for activation fn
    """

    def __init__(self, name):
        self.name = name

    def __call__(self, x):
        """
        Calculates activation function output
        :param x: Activation fn input
        :return: f(x)
        """
        pass

    def df(self, x):
        """
        Calculates activation function derivative
        :param x: Derivative argument
        :return: f'(x)
        """
        pass

class ReLU(Activation):

    def __init__(self):
        self.name = 'ReLU'

    def __call__(self, x):
        return max(0, x)

    def df(self, x):
        if x <= 0:
            return 0
        else:
            return 1

class TanH(Activation):

    def __init__(self):
        self.name = 'TanH'

    def __call__(self, x):
        return np.tanh(x)

    def df(self, x):
        return 1 - np.power(self(x),2)

class ATan(Activation):
    def __init__(self):
        self.name = 'ATan'

    def __call__(self, x):
        return np.arctan(x)

    def df(self, x):
        return 1/(x**2 + 1)

class Sigmoid(Activation):

    def __init__(self):
        self.name = 'Sigmoid'

    def __call__(self, x):
        return 1/(1 + np.exp(-x))

    def df(self, x):
        return self(x)*(1-self(x))

class Linear(Activation):

    def __init__(self):
        self.name = 'Linear'

    def __call__(self, x):
        return x

    def df(self, x):
        return 1