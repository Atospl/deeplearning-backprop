## Implementacja wielowarstwowego perceptronu (MLP) uczonego algorytmem propagacji wstecznej 

> Własnoręczna implementacja sieci, dozwolone jest użycie wyłącznie  
> podstawowych pakietów (**NumPy**, pandas, itp.).

## DEADLINE

Wstępne oddanie projektu - częściowy raport, częściowa implementacja - **17.03.2020**
Ostateczne oddanie projektu - **24.03.2020**

### Parametrami sieci MLP są:
- [ ] Liczba warstw i neuronów ukrytych w każdej warstwie
- [ ] Funkcja aktywacji
- [ ] Obecność biasu
- [ ] Rozmiar batcha (Online vs batch learning)
- [ ] Liczba iteracji
- [ ] Wartość współczynnika nauki
- [ ] Wartość współczynnika bezwładności
- [ ] Rodzaj problemu: klasyfikacja lub regresja


### Inne wymagania:
- [ ] Program powinien mieć możliwość zainicjowania powtarzalnego  
procesu uczenia poprzez podania stałego ziarna generatora liczb  
losowych

- [ ] Na wstępnym oddaniu projektu należy pokazać chociaż częściowo  
przygotowany raport

- [ ] Aplikacja prezentuje w formie wykresu błąd sieci na zbiorze uczącym i testowym

- [ ] Możliwość prześledzenia procesu uczenia iteracja po iteracji (wizualizacja wag krawędzi)

- [ ] Wizualizacja błędu propagowanego w kolejnych                   
iteracjach uczenia (na każdej z wag)

- [ ] Wizualizacja błędu propagowanego w kolejnych iteracjach uczenia (na każdej z wag)

- [ ] Wizualizacja zbioru uczącego oraz rezultatów klasyfikacji/regresji

- [ ] Student powinien umieć wyjaśnić przyczynę sukcesu lub porażki  
danego modelu przy zadanych przez prowadzącego parametrach  
sieci i zadania (możliwość sprawnej zmiany parametrów sieci oraz zbiorów treningowych i testowych)


- [ ] Druga część projektu polega na optymalizacji sieci w zadaniu rozpoznawania cyfr  
(https://www.kaggle.com/c/digit-recognizer/)

- [ ] zyskany wynik powinien być zarejestrowany na platformie kaggle

### Elementy do zbadania:
- [ ] Wpływ funkcji aktywacji na skuteczność działania sieci - sprawdzić funkcję  
sigmoidalną i dwie inne, dowolne funkcje aktywacji wewnątrz sieci.  
Funkcja aktywacji na wyjściu musi być dobrana odpowiednio do rodzaju problemu
- [ ] Wpływ liczby warstw ukrytych w sieci i ich liczności. Zbadać różne  
liczby warstw od 0 do 4, kilka różnych architektur
- [ ] Wpływ funkcji błędu na wyjściu sieci na skuteczność uczenia.  
Sprawdzić dwie funkcje błędu dla klasyfikacji i dwie dla regresji.

